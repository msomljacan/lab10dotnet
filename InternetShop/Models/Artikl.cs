﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace InternetShop.Models
{
    public class Artikl
    {
        [Key]
        public int id { get; set; }
        public string naziv { get; set; }
        public string opis { get; set; }
        public int cijena { get; set; }
        public int dostupnaKolicina { get; set; }
        public bool akcija { get; set; }
        public int akcijskaCijena{get;set;}
    }
}
